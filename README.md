# SoaInt Test

Observaciones y recomendaciones sobre el código enviado como evaluación.

## Archivos para la evaluación

- [Backend](./archivos/backend.java)
- [Frontend](./archivos/frontend.jsx)

## Observaciones y recomendaciones

### Backend

- Observaciones

  1. La funcionalidad no aplica una única responsabilidad: Realiza el log a multiples salidas (archivos, consola, base de datos) y a la vez su propia configuración.
  2. La funcionalidad presenta multiples estructuras condicionales. Esto va de la mano con el tener que manejar multiples responsabilidades. Esto aumenta en gran medida la complejida ciclomática de la clase.
  3. En uno de los métodos se abre una conexión a la base de datos y no se cierra luego de su uso.
  4. No se hace uso de la clase PreparedStatement para escapar los parámetros y esto es una potencial objetivo para un ataque de inyección sql.
  5. Se crean instancias de la clase Date y DateFormat en las líneas 84, 88 y 92 en lugar de asignarlo a una variable y hacer reutilización.
  6. No se usan nombres de variables descriptivas en las líneas 59 y 74.
  7. El nombre de los métodos no siguen la convención camelCase.

- Recomendaciones
  1. Aplicar el principio de responsabilidad única (Solid) y separar las responsabilidades de configuración del logger y la implementación para diferentes salidas en diferentes clases.
  2. Aplicar el principio de sustitución de Liskov (soLid) y crear una interfaz que implementen todas las clases que representen una nueva salida (Consola, archivos, base de datos)
  3. Usar nombre de variables que tengan un valor en el contexto de uso. Esto ayudará a poder entender el código de una manera más sencilla en su modificación y mantención.
  4. Siempre cerrar los recursos que se usen en la aplicación y así evitar fugas de memoria y sobrecarga de conexiones a la base de datos.
  5. Asignar valores repetibles a variables pero siempre tener como prioridad la legibilidad.
  6. Aplicar pruebas unitarias al código. Esto salvaguardará que se generen bugs por modificaciones y/o inclusión de nuevas funcionalidades al código existente.

### Frontend

- Observaciones

  1. Usar componenentes funcionales en lo posible.
  2. Separar la lógica de consumo de servicios de la de renderizado de valores.
  3. Uso de una cadena que puede variar en diferentes ambientes en la línea 11.

- Recomendaciones
  1. Crear un componente presentacional para mostrar los valores. Estos componentes serán independientes de la obtención de la data.
  2. Crear un componente contenedor que se encargue de la lógica y obtención de data de servicios y la reasigne al componente presentacional.
  3. Usar variables de entorno para valores que pueden cambiar en diferentes entornos (desarrollo, producción) y así evitar modificar el código por esa necesidad.

---

Hecho con 💙
